package telxlclient

import (
	"encoding/json"
	"fmt"

	"../utils"
)

//ProvisionResultEvent -
type ProvisionResultEvent struct {
	Event struct {
		EventType string            `json:"event_type"`
		Results   []ProvisionResult `json:"results"`
	} `json:"event"`
}

//ProvisionResult -
type ProvisionResult struct {
	Active       string `json:"active"`
	CreateDate   string `json:"create_date"`
	CreatedBy    string `json:"created_by"`
	ExternalID   string `json:"external_id"`
	ID           string `json:"id"`
	ModifiedDate string `json:"modified_date"`
	ParentID     string `json:"parent_id"`
	ParentTable  string `json:"parent_table"`
	Sequence     string `json:"sequence"`
	Table        string `json:"table"`
	Title        string `json:"title"`
}

//ProvisioningListHandler
func provisioningListHandler(event string) {

	e := ProvisionResultEvent{}
	json.Unmarshal([]byte(event), &e)

	// Print Header
	fmt.Println("")
	fmt.Printf(utils.White + "| active     " + utils.Reset)
	fmt.Printf(utils.White + "| create_date          " + utils.Reset)
	fmt.Printf(utils.White + "| created_by " + utils.Reset)
	fmt.Printf(utils.White + "| external_id  " + utils.Reset)
	fmt.Printf(utils.White + "| id         " + utils.Reset)
	fmt.Printf(utils.White + "| modified_date        " + utils.Reset)
	fmt.Printf(utils.White + "| parent_id  " + utils.Reset)
	fmt.Printf(utils.White + "| parent_table         " + utils.Reset)
	fmt.Printf(utils.White + "| sequence   " + utils.Reset)
	fmt.Printf(utils.White + "| table                " + utils.Reset)
	fmt.Printf(utils.White + "| title                        " + utils.Reset)
	fmt.Println("|")

	// Print a Nice Table of the Results.
	for _, v := range e.Event.Results {
		fmt.Printf("| %-10v ", v.Active)
		fmt.Printf("| %-20v ", v.CreateDate)
		fmt.Printf("| %-10v ", v.CreatedBy)
		fmt.Printf("| %-12v ", v.ExternalID)
		fmt.Printf("| %-10v ", v.ID)
		fmt.Printf("| %-20v ", v.ModifiedDate)
		fmt.Printf("| %-10v ", v.ParentID)
		fmt.Printf("| %-20v ", v.ParentTable)
		fmt.Printf("| %-10v ", v.Sequence)
		fmt.Printf("| %-20v ", v.Table)
		fmt.Printf("| %-28v ", v.Title)

		fmt.Println("|")
	}
	fmt.Println("")
}

// ProvisionValuesEvent -
type ProvisionValuesEvent struct {
	Event struct {
		ID             string       `json:"id"`
		EventType      string       `json:"event_type"`
		ExternalID     string       `json:"external_id"`
		ExternalIDType string       `json:"external_id_type"`
		RequestID      string       `json:"request_id"`
		Table          string       `json:"table"`
		NamesandValues []NameValues `json:"names_and_values"`
	} `json:"event"`
}

//NameValues -
type NameValues struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

//ProvisionValuesHandler -
func provisioningValuesHandler(event string) {
	fmt.Println("Event_Values Handler")

	e := ProvisionValuesEvent{}
	//UnmarshallEvent and pass in value
	json.Unmarshal([]byte(event), &e)

	//uncomment to show relevant values
	// utils.Inbound(utils.FormattedJSON(event))
	utils.Event("[" + e.Event.EventType + "]")

	// Print Header of Event Table Values
	fmt.Printf(utils.White + "| id      " + utils.Reset)
	fmt.Printf(utils.White + "| event_type   " + utils.Reset)
	fmt.Printf(utils.White + "| external_id  	" + utils.Reset)
	fmt.Printf(utils.White + "| external_id_type 	" + utils.Reset)
	fmt.Printf(utils.White + "| request_id 	" + utils.Reset)
	fmt.Printf(utils.White + "| table 		" + utils.Reset)
	fmt.Println("|")

	//Print a nice table of eventValues
	fmt.Printf("|%-10v ", e.Event.ID)
	fmt.Printf("|%-15v ", e.Event.EventType)
	fmt.Printf("|%-15v ", e.Event.ExternalID)
	fmt.Printf("|%-20v ", e.Event.ExternalIDType)
	fmt.Printf("|%-15v ", e.Event.RequestID)
	fmt.Printf("|%-15v ", e.Event.Table)
	fmt.Println("|")

	// Print Header of Name Table Values
	fmt.Printf(utils.White + "| Name  			      " + utils.Reset)
	fmt.Printf(utils.White + "| Value 	" + utils.Reset)
	fmt.Println("|")

	// Print a Nice Table of content of  Names & values.
	for _, nV := range e.Event.NamesandValues {

		fmt.Printf("| %-20v ", nV.Name)
		fmt.Printf("| %-20v ", nV.Value)

		fmt.Println("|")
	}
	fmt.Println("")

}

//TitleEvent -
type TitleEvent struct {
	Event struct {
		EventType      string            `json:"event_type"`
		RequestID      string            `json:"request_id"`
		ExternalIDType string            `json:"external_id_type"`
		Table          string            `json:"table"`
		ID             string            `json:"id"`
		ExternalID     string            `json:"external_id"`
		NamesAndValues []NameValuesTitle `json:"names_and_values"`
	} `json:"event"`
}

//NameValuesTitle -
type NameValuesTitle struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

//ProvisioningTitleHandler -
func provisionTitleHandler(event string) {
	fmt.Println("Provisioning_Title_Handler")

	// If you want to see all the JSON Uncomment this line
	t := TitleEvent{}
	json.Unmarshal([]byte(event), &t)

	utils.Event("[" + t.Event.EventType + "]")
	// utils.Event("[" + t.Event.Title + "]")

	// Print Header of Event Table Values
	fmt.Printf(utils.White + "| id       " + utils.Reset)
	fmt.Printf(utils.White + "| event_type     		  " + utils.Reset)
	fmt.Printf(utils.White + "| external_id       " + utils.Reset)
	fmt.Printf(utils.White + "| external_id_type       " + utils.Reset)
	fmt.Printf(utils.White + "| request_id      " + utils.Reset)
	fmt.Printf(utils.White + "| table 			" + utils.Reset)
	fmt.Println("|")

	//Print a nice table of eventValues
	fmt.Printf("| %-10v ", t.Event.ID)
	fmt.Printf("| %-25v ", t.Event.EventType)
	fmt.Printf("| %-10v ", t.Event.ExternalID)
	fmt.Printf("| %-12v ", t.Event.ExternalIDType)
	fmt.Printf("| %-10v ", t.Event.RequestID)
	fmt.Printf("| %-28v ", t.Event.Table)
	fmt.Println("|")

	// Print Header of Name Table Values
	fmt.Printf(utils.White + "| Name        " + utils.Reset)
	fmt.Printf(utils.White + "| Value        " + utils.Reset)
	fmt.Println("|")

	// Print a Nice Table of content of  Names & values.
	for _, nVT := range t.Event.NamesAndValues {

		fmt.Printf("| %-20v ", nVT.Name)
		fmt.Printf("| %-20v ", nVT.Value)

		fmt.Println("|")
	}
	fmt.Println("")
}

//ProvisionTelephonyValuesEvent -
type ProvisionTelephonyValuesEvent struct {
	Event struct {
		EventType         string        `json:"event_type"`
		RequestID         string        `json:"request_id"`
		SpecificServiceID int           `json:"specific_service_id"`
		OnlyActive        int           `json:"only_active"`
		ExternalIDType    string        `json:"external_id_type"`
		Services          []Services    `json:"services"`
		Numbers           []Numbers     `json:"numbers"`
		Queues            []Queues      `json:"queues"`
		OpsAssigned       []OpsAssigned `json:"ops_assigned"`
	} `json:"event"`
}

//Services -
type Services struct {
	ServiceTable     string `json:"service_table"`
	ServiceID        int    `json:"service_id"`
	ExternalID       string `json:"external_id"`
	ServiceActive    int    `json:"service_active"`
	ServiceNodeTable string `json:"service_node_table"`
	ServiceNodeID    int    `json:"service_node_id"`
	ServiceNodeTitle string `json:"service_node_title"`
}

//Numbers -
type Numbers struct {
	NumberTable              string `json:"number_table"`
	NumberID                 int    `json:"number_id"`
	ExternalID               string `json:"external_id"`
	NumberRealDdi            string `json:"number_real_ddi"`
	NumberPresentationNumber string `json:"number_presentation_number"`
}

//Queues -
type Queues struct {
	QueueTable string `json:"queue_table"`
	QueueID    string `json:"queue_id"`
	ExternalID string `json:"external_id"`
	QueueTitle string `json:"queue_title"`
}

//OpsAssigned -
type OpsAssigned struct {
	Level     string        `json:"level"`
	SetFromDc string        `json:"set_from_dc"`
	Groups    []interface{} `json:"groups"`
	Operators []interface{} `json:"operators"`
}

//ProvisioningTelephonyServiceHandler
func provisioningTelephonyServicesHandler(event string) {

	fmt.Println("Provisioning_Telephony_Service_Handler")

	e := ProvisionTelephonyValuesEvent{}

	// If you want to see all the JSON Uncomment this line
	// utils.Inbound(utils.FormattedJSON(event))

	json.Unmarshal([]byte(event), &e)

	utils.Event("[" + e.Event.EventType + "]")

	// Print Header of Event Table Values
	fmt.Printf(utils.White + "| event_type   		   " + utils.Reset)
	fmt.Printf(utils.White + "| request_id      " + utils.Reset)
	fmt.Printf(utils.White + "| specific_service_id     " + utils.Reset)
	fmt.Printf(utils.White + "| external_id_type       " + utils.Reset)
	fmt.Printf(utils.White + "| only_active 			" + utils.Reset)
	fmt.Println("|")

	//Print a nice table of EventValues
	fmt.Printf("| %-20v ", e.Event.EventType)
	fmt.Printf("| %-10v ", e.Event.RequestID)
	fmt.Printf("| %-25v ", e.Event.SpecificServiceID)
	fmt.Printf("| %-12v ", e.Event.ExternalIDType)
	fmt.Printf("| %-10v ", e.Event.OnlyActive)
	fmt.Println("|")

	// Print Header of Content Services Values
	fmt.Println("")
	fmt.Printf(utils.White + "| service_table       " + utils.Reset)
	fmt.Printf(utils.White + "| service_id       " + utils.Reset)
	fmt.Printf(utils.White + "| external_id       " + utils.Reset)
	fmt.Printf(utils.White + "| service_active      " + utils.Reset)
	fmt.Printf(utils.White + "| service_node_table     " + utils.Reset)
	fmt.Printf(utils.White + "| service_node_id     " + utils.Reset)
	fmt.Printf(utils.White + "| service_node_title     " + utils.Reset)
	fmt.Println("|")
	fmt.Println("")

	// Print a Nice Table of Content Services Values.
	for _, sV := range e.Event.Services {

		fmt.Printf("| %-20v ", sV.ServiceTable)
		fmt.Printf("| %-20v ", sV.ServiceID)
		fmt.Printf("| %-20v ", sV.ExternalID)
		fmt.Printf("| %-20v ", sV.ServiceActive)
		fmt.Printf("| %-20v ", sV.ServiceNodeTable)
		fmt.Printf("| %-20v ", sV.ServiceNodeID)
		fmt.Printf("| %-20v ", sV.ServiceNodeTitle)

		fmt.Println("|")
	}

	// Print Header of Number Values
	fmt.Printf(utils.White + "| number_table       " + utils.Reset)
	fmt.Printf(utils.White + "| number_id      " + utils.Reset)
	fmt.Printf(utils.White + "| external_id      " + utils.Reset)
	fmt.Printf(utils.White + "| number_real_ddi       " + utils.Reset)
	fmt.Printf(utils.White + "| number_presentation_number      " + utils.Reset)
	fmt.Println("|")
	fmt.Println("")

	for _, V := range e.Event.Numbers {

		fmt.Printf("| %-20v ", V.NumberTable)
		fmt.Printf("| %-20v ", V.NumberID)
		fmt.Printf("| %-20v ", V.ExternalID)
		fmt.Printf("| %-20v ", V.NumberRealDdi)
		fmt.Printf("| %-20v ", V.NumberPresentationNumber)

		fmt.Println("|")
	}
	fmt.Println("")

}

//CreateEvent -
type CreateEvent struct {
	Event struct {
		EventType      string             `json:"event_type"`
		RequestID      string             `json:"request_id"`
		ExternalIDType string             `json:"external_id_type"`
		Table          string             `json:"table"`
		ID             string             `json:"id"`
		ExternalID     string             `json:"external_id"`
		NamesAndValues []NameValuesCreate `json:"names_and_values"`
	} `json:"event"`
}

//NameValuesCreate -
type NameValuesCreate struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

//ProvisioningCreateHandler -
func provisioningCreateHandler(event string) {
	fmt.Println("Provisioning_Create_Handler")

	// If you want to see all the JSON Uncomment this line
	c := CreateEvent{}
	json.Unmarshal([]byte(event), &c)

	utils.Event("[Provisioning_Create]")

	// Print Header of Event Table Values
	fmt.Printf(utils.White + "| id       " + utils.Reset)
	fmt.Printf(utils.White + "| event_type     		  " + utils.Reset)
	fmt.Printf(utils.White + "| external_id       " + utils.Reset)
	fmt.Printf(utils.White + "| external_id_type       " + utils.Reset)
	fmt.Printf(utils.White + "| request_id      " + utils.Reset)
	fmt.Printf(utils.White + "| table 			" + utils.Reset)
	fmt.Println("|")

	//Print a nice table of eventValues
	fmt.Printf("| %-10v ", c.Event.ID)
	fmt.Printf("| %-25v ", c.Event.EventType)
	fmt.Printf("| %-10v ", c.Event.ExternalID)
	fmt.Printf("| %-12v ", c.Event.ExternalIDType)
	fmt.Printf("| %-10v ", c.Event.RequestID)
	fmt.Printf("| %-28v ", c.Event.Table)
	fmt.Println("|")

	// Print Header of Name Table Values
	fmt.Printf(utils.White + "| Name        " + utils.Reset)
	fmt.Printf(utils.White + "| Value        " + utils.Reset)
	fmt.Println("|")

	// Print a Nice Table of content of  Names & values.
	for _, nV := range c.Event.NamesAndValues {

		fmt.Printf("| %-20v ", nV.Name)
		fmt.Printf("| %-20v ", nV.Value)

		fmt.Println("|")
	}
	fmt.Println("")
}

//DeactivateEvent -
type DeactivateEvent struct {
	Event struct {
		EventType      string                 `json:"event_type"`
		RequestID      string                 `json:"request_id"`
		ExternalIDType string                 `json:"external_id_type"`
		Table          string                 `json:"table"`
		ID             string                 `json:"id"`
		ExternalID     string                 `json:"external_id"`
		NamesAndValues []NameValuesDeactivate `json:"names_and_values"`
	} `json:"event"`
}

//NameValuesDeactivate -
type NameValuesDeactivate struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

//ProvisioningDeactivateHandler
func provisioningDeactivateHandler(event string) {
	fmt.Println("Provisioning_Deactivate_Handler")

	// If you want to see all the JSON Uncomment this line
	d := DeactivateEvent{}
	json.Unmarshal([]byte(event), &d)

	utils.Event("[" + d.Event.EventType + "]")

	// Print Header of Event Table Values
	fmt.Printf(utils.White + "| event_type     		  " + utils.Reset)
	fmt.Printf(utils.White + "| request_id      " + utils.Reset)
	fmt.Printf(utils.White + "| external_id_type       " + utils.Reset)
	fmt.Printf(utils.White + "| table 			" + utils.Reset)
	fmt.Printf(utils.White + "| id       " + utils.Reset)
	fmt.Printf(utils.White + "| external_id       " + utils.Reset)
	fmt.Println("|")

	//Print a nice table of eventValues
	fmt.Printf("| %-10v ", d.Event.ID)
	fmt.Printf("| %-25v ", d.Event.EventType)
	fmt.Printf("| %-10v ", d.Event.ExternalID)
	fmt.Printf("| %-12v ", d.Event.ExternalIDType)
	fmt.Printf("| %-10v ", d.Event.RequestID)
	fmt.Printf("| %-28v ", d.Event.Table)
	fmt.Println("|")

	// Print Header of Name Table Values
	fmt.Printf(utils.White + "| Name        " + utils.Reset)
	fmt.Printf(utils.White + "| Value        " + utils.Reset)
	fmt.Println("|")

	// Print a Nice Table of content of  Names & values.
	for _, nV := range d.Event.NamesAndValues {

		fmt.Printf("| %-20v ", nV.Name)
		fmt.Printf("| %-20v ", nV.Value)

		fmt.Println("|")
	}
	fmt.Println("")
}
