package telxlclient

import (
	"encoding/json"

	"../utils"
)

type agent struct {
	name       string
	email      string
	operatorID int
	loginState int
	stateCode  int
	stateDesc  string
	target     string
	wrapTime   int
}

var (
	agents = map[int]agent{}
)

// OpStatusEventType -
type OpStatusEventType struct {
	Event struct {
		BreakID     string `json:"breakID"`
		BreakName   string `json:"breakName"`
		CustomerID  string `json:"customerID"`
		Email       string `json:"email"`
		EventType   string `json:"event_type"`
		LoginState  string `json:"login_state"`
		Name        string `json:"name"`
		OpStateCode string `json:"op_state_code"`
		OpStateDesc string `json:"op_state_desc"`
		OperatorID  string `json:"operatorID"`
		RingTime    string `json:"ring_time"`
		Target      string `json:"target"`
		WrapTime    string `json:"wrap_time"`
	} `json:"event"`
}

func opStatusHandler(event string) {

	//utils.Handler(utils.White + "handling Operator Status Event." + utils.Reset)

	// If you want to see all the JSON Uncomment this line
	//utils.Inbound(utils.FormattedJSON(event))

	e := OpStatusEventType{}
	json.Unmarshal([]byte(event), &e)

	utils.Event("[" + e.Event.EventType + "] Name: " + e.Event.Name + " Status:" + e.Event.OpStateDesc)

}
