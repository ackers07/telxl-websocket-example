package telxlclient

import (
	"fmt"
	"time"
)

//Agent1 -
func (c *Client) Agent1() {
	/* login as the agent
	read the target number - is this possible in the API?
	change the target number to something new
	read it again to verify it has changed
	update it back
	logout
	*/

	c.send(operatorLoginCmd(55744750))
	time.Sleep(1 * time.Second)
	c.send(updateTargetCmd(55744750, "07872561095"))
	time.Sleep(1 * time.Second)
	c.send(updateTargetCmd(55744750, "07866765136"))
	time.Sleep(1 * time.Second)
	c.send(operatorLogoutCmd(55744750))

}

func operatorLoginCmd(operatorID int) string { //todo: where do we get the agent params from? golden files?
	return fmt.Sprintf(`{ "command": { "command_type": "op_login", "operator_id":%d} }`, operatorID)
}

func operatorLogoutCmd(operatorID int) string { //todo: where do we get the agent params from? golden files?
	return fmt.Sprintf(`{ "command": { "command_type": "op_logout", "operator_id":%d} }`, operatorID)
}

func updateTargetCmd(operatorID int, newTarget string) string { //07866765136
	return fmt.Sprintf(`{ "command": { "command_type": "op_update_target", "operator_id": %d, "target": "%s" } }`, operatorID, newTarget)
}
