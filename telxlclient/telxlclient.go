package telxlclient

import (
	"net/url"

	"../utils"
	"github.com/gorilla/websocket"
)

// Client -
type Client struct {
	Websocketconnection *websocket.Conn
	Username            string
	Password            string
	AgentID             string
	ConfigID            string
}

// Connect -
func (c *Client) Connect(urlhost, urlpath, username, password string, agentid string, configid string) {

	u := url.URL{Scheme: "wss", Host: urlhost, Path: urlpath}
	utils.Info("Connecting to " + u.String())

	con, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	utils.Fail(err, "Failed to Connect")
	c.Websocketconnection = con
	c.Username = username
	c.Password = password
	c.AgentID = agentid
	c.ConfigID = configid

	utils.Info("Connected")

	// Start the Receiving Thread
	go c.Receive()

}

// Disconnect -
func (c *Client) Disconnect() {
	c.Websocketconnection.Close()
}

// StartSession -
func (c *Client) StartSession(release string) {
	command := `{"command":{"command_type":"session_start","user":"` + c.Username + `","pass":"` + c.Password + `","agent_id":` + c.AgentID + `,"config_id":` + c.ConfigID + `,"mode":"api","version_string": "` + release + `"}}`
	c.send(command)
}

// EndSession -
func (c *Client) EndSession() {
	command := `{"command":{"command_type":"session_end","reason":"End of test"}}`
	c.send(command)
}

// Receive - Receive all the Inbound Information
func (c *Client) Receive() {

	// As this was called from a go routine, put the thread into an infinate loop
	// and try and decode anything coming into the websocket.
	for {

		// TODO: Handle the Function Gracefully
		// Is the Connection Still Open? This loop as no idea? and the Program won't exit from the Main loop until all the
		// Threads of completed.

		_, message, err := c.Websocketconnection.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				utils.Fail(err, "Failed to Read Message")
			}
			return
		}

		// TODO
		// grab the event type from the JSON and Process it
		HandleEvent(string(message))
	}
}

// SendPing -
func (c *Client) SendPing() {
	command := `{"command" : {"command_type":"session_ping"}}`
	c.send(command)
}

// SendSessionTest -
func (c *Client) SendSessionTest() {
	command := `{"command":{"command_type":"session_test"}}`
	c.send(command)
}

//SendProvisioningRequestList -
func (c *Client) SendProvisioningRequestList() {

	// Provisioning Details can have more than one table in the commaand
	command := `{"command":{"command_type":"provisioning_request_list","table":"caco_numbers,db_users"}}`
	c.send(command)

	command = `{"command":{"command_type":"provisioning_request_list","table":"ut_accessNumber"}}`
	c.send(command)

	command = `{"command":{"command_type":"provisioning_request_list","table":"vcc_operators"}}`
	c.send(command)

	command = `{"command":{"command_type":"provisioning_request_list","table":"vcc_operatorGroups"}}`
	c.send(command)

}

//SendProvisioningRequestTelephonyService -
func (c *Client) SendProvisioningRequestTelephonyService() {

	//Provisioning request of telephony services
	command := `{"command" : {"command_type":"provisioning_request_telephony_services"}}`
	c.send(command)

	//request with a request_id
	command = `{"command" : {"command_type":"provisioning_request_telephony_services", "request_id": "<request_id>"}}`
	c.send(command)

	//Specifying a service
	command = `{"command" : {"command_type":"provisioning_request_telephony_services", "specific_service_id":"<service_ID>"}}`
	c.send(command)

	//Show inactive services
	command = `{"command" : {"command_type":"provisioning_request_telephony_services", "only_active":"0"}}`
	c.send(command)

	//Show only active services
	command = `{"command" : {"command_type":"provisioning_request_telephony_services", "only_active":"1"}}`
	c.send(command)

	//requesting only on external_id_type
	command = `{"command" : {"command_type":"provisioning_request_telephony_services","external_id_type":"<external_id_type>"}}`
	c.send(command)

	//do not display external_id_type
	command = `{"command" : {"command_type":"provisioning_request_telephony_services","external_id_type":"<not_my_external_id_type>"}}`
	c.send(command)

}

//SendProvisioningValues -
//Getting values for provisioning_set_values
//GIVEN that your customer has operator queues and operator groups
func (c *Client) SendProvisioningValues() {

	// table specific to both operators and groups
	command := `{"command":{"command_type":"provisioning_get_values","table":"ut_operatorQueue","id":` + c.AgentID + `}}`
	c.send(command)

	command = `{"command":{"command_type":"provisioning_get_values","table":"vcc_operatorGroups","id":"6208"}}`
	c.send(command)

}

// SendProvisionTitle -
//Access to set data, Change the Title of an OP  and/or with Special chacters Special chacters
func (c *Client) SendProvisionTitle(table string, title string) {

	//Provision title for op
	command := `{"command":{"command_type":"provisioning_set_title","table":"` + table + `","id":` + c.AgentID + `,"title":"` + title + `"}}`
	c.send(command)

}

// SendProvisioningCreate -
//Access to set data, and create new Op/Agent and Op Group
func (c *Client) SendProvisioningCreate() {

	//Provision create new Op Remember- "web_username" has to be unique or will cause a provisioning_error
	command := `{"command":{"command_type":"provisioning_create","table":"vcc_operators","request_id":"create user 5","external_id_type":"SIT Testing","external_id":"SIT11","use_template_id":55776085,"title":"Simba New Testy","web_username":"Simba testy 10"}}`
	c.send(command)

	//Provision create new Op queue
	command = `{"command":{"command_type":"provisioning_create","table":"vcc_operatorGroups","request_id":"SIT11","external_id_type":"SIT Testing","external_id":"test_groups_add","title":"Group 3","notes":"Testing creation of groups from api"}}`
	c.send(command)

}

// SendProvisionDeactivate -
func (c *Client) SendProvisionDeactivate() {

	//Deactivate Op GIVEN you have created an operator
	command := `{"command":{"command_type":"provisioning_deactivate","table":"vcc_operators","request_id":"retire user 5","external_id_type":"SIT Testing","external_id":"SIT11"}}`
	c.send(command)

	//Deactivating a Op Group Given you have created a Group
	command = `{"command":{"command_type":"provisioning_deactivate","table":"vcc_operatorGroups","request_id":"SIT11","external_id_type":"SIT Testing","external_id":"test_groups_add"}}`
	c.send(command)

}

func (c *Client) send(command string) {
	utils.Outbound(command)
	c.Websocketconnection.WriteMessage(websocket.TextMessage, []byte(command))
}
