package telxlclient

import (
	"encoding/json"

	"../utils"
)

//SessionStartResultHandler -
func sessionStartResultHandler(event string) {

	// If you want to see all the JSON Uncomment this line
	utils.Inbound(utils.FormattedJSON(event))

	var evJSON map[string]interface{}
	json.Unmarshal([]byte(event), &evJSON)
	ev := evJSON["event"].(map[string]interface{})
	//	resultCode, _ := ev["resultCode"].(float64)

	if ev["result_code"] == nil || 200 != int(ev["result_code"].(float64)) {
		utils.Exit(1, "[session_start_result] bad Result Code, event: "+utils.FormattedJSON(event))
	}

	//msg := fmt.Sprintf("%.0f %v, sessionID=%.0f", ev["result_code"].(float64), ev["resultText"].(string), ev["sessionID"].(float64))
	//utils.Event("[session_start_result] " + msg)
}

// SessionPongHandler -
func sessionPongHandler(event string) {

	// If you want to see all the JSON Uncomment this line
	//utils.Inbound(utils.FormattedJSON(event))

	type pong struct {
		Event struct {
			EventType string `json:"event_type"`
		} `json:"event"`
	}

	p := pong{}
	json.Unmarshal([]byte(event), &p)

	if p.Event.EventType != "session_pong" {
		utils.Exit(1, "bad pong:"+utils.FormattedJSON(event))
	}

	utils.Event("[session_pong]")
}
