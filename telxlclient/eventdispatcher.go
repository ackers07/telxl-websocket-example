package telxlclient

import (
	"encoding/json"

	"../utils"
)

var (
	handlers = map[string]func(string){

		"session_pong":                    sessionPongHandler,
		"session_start_result":            sessionStartResultHandler,
		"error":                           errorhandler,
		"op_status":                       opStatusHandler,
		"provisioning_list":               provisioningListHandler,
		"provisioning_set_title":          provisionTitleHandler,
		"provisioning_values":             provisioningValuesHandler,
		"provisioning_telephony_services": provisioningTelephonyServicesHandler,
		"provisioning_create":             provisioningCreateHandler,
		"provisioning_deactivate":         provisioningDeactivateHandler,
	}
)

//EventType -
type EventType struct {
	Event struct {
		EventType string `json:"event_type"`
	} `json:"event"`
}

//HandleEvent -
func HandleEvent(rawEvent string) {
	var e EventType
	json.Unmarshal([]byte(rawEvent), &e)
	//log.Printf("EventType is %s",e.Event.EventType)

	// do we have a handler for this event?
	handler, found := handlers[e.Event.EventType]
	if !found {
		//use the default handler
		defaulthandler(e.Event.EventType)
		// Output the Raw Event.
		utils.Inbound(utils.FormattedJSON(string(rawEvent)))
		return
	}

	handler(rawEvent) //pass in the raw event

}

func errorhandler(event string) {
	utils.Handler("handling error")
	utils.Inbound(utils.FormattedJSON(event)) //useful to see any errors for now
}

func defaulthandler(event string) {
	utils.Handler("no handler for " + event + " event exists yet")
}
