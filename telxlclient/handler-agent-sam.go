package telxlclient

import (
	"fmt"
	"time"
)

//AgentSam -
func (c *Client) AgentSam() {
	/* login as the agent
	read the target number - is this possible in the API?
	change the target number to something new
	read it again to verify it has changed
	update it back
	logout
	*/

	c.send(operatorSamLoginCmd(55777796))
	time.Sleep(1 * time.Second)
	c.send(updateSamTargetCmd(55777796, "07805061205"))
	time.Sleep(1 * time.Second)
	c.send(updateSamTargetCmd(55777796, "07710255637"))
	time.Sleep(1 * time.Second)
	c.send(operatorSamLogoutCmd(55777796))
}

func operatorSamLoginCmd(vccoperatorID int) string { //todo: where do we get the agent params from? golden files?
	return fmt.Sprintf(`{ "command": { "command_type": "op_login", "vcc_operator_id":%d} }`, vccoperatorID)
}

func operatorSamLogoutCmd(vccoperatorID int) string { //todo: where do we get the agent params from? golden files?
	return fmt.Sprintf(`{ "command": { "command_type": "op_logout", "vcc_operator_id":%d} }`, vccoperatorID)
}

func updateSamTargetCmd(vccoperatorID int, newTarget string) string { //07805061205
	return fmt.Sprintf(`{ "command": { "command_type": "op_update_target", "vcc_operator_id": %d, "target": "%s" } }`, vccoperatorID, newTarget)
}
