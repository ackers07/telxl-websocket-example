package telxlclient

import (
	"encoding/json"
	"fmt"

	"../utils"
)

// ProvisioningErrorHandler -
func provisioningErrorHandler(event string) {

	type provisioningErrorHandler struct {
		Event struct {
			EventType string `json:"event_type"`
		} `json:"event"`
	}
	fmt.Println("handler provisioning error entry point")
	pEH := provisioningErrorHandler{}
	json.Unmarshal([]byte(event), &pEH)

	if pEH.Event.EventType != "provisioning_error" {
		utils.Exit(1, "bad provisioning request: "+utils.FormattedJSON(event))
	}

	//utils.Inbound(utils.FormattedJSON(event))
	utils.Event("[provisioning_error]")
}
