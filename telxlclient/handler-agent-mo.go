package telxlclient

import (
	"fmt"
	"time"
)

//AgentMo -
func (c *Client) AgentMo() {
	/* login as the agent
	read the target number - is this possible in the API?
	change the target number to something new
	read it again to verify it has changed
	update it back
	logout
	*/
	c.send(operatorMoLoginCmd(55777809))
	time.Sleep(1 * time.Second)
	c.send(updateMoTargetCmd(55777809, "07453968825"))
	time.Sleep(1 * time.Second)
	c.send(updateMoTargetCmd(55777809, "07866765136"))
	time.Sleep(1 * time.Second)
	c.send(operatorMoLogoutCmd(55777809))
}

func operatorMoLoginCmd(operatorID int) string { //todo: where do we get the agent params from? golden files?
	return fmt.Sprintf(`{ "command": { "command_type": "op_login", "operator_id":%d} }`, operatorID)
}

func operatorMoLogoutCmd(operatorID int) string { //todo: where do we get the agent params from? golden files?
	return fmt.Sprintf(`{ "command": { "command_type": "op_logout", "operator_id":%d} }`, operatorID)
}

func updateMoTargetCmd(operatorID int, newTarget string) string { //07866765136
	return fmt.Sprintf(`{ "command": { "command_type": "op_update_target", "operator_id": %d, "target": "%s" } }`, operatorID, newTarget)
}
