package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"./config"
	"./telxlclient"
	"./utils"
)

func main() {

	// Decode the Command Line arguments to the program.
	//
	// go run main.go -user 5xxxxxx -pass xxxx -api list
	// or (when compiled)
	// ./program -user 5xxxxxx -pass xxxx -api list
	//
	user := flag.String("user", "TelXL-Username", "Your TelXL User name. It's noramally a number")
	pass := flag.String("pass", "TelXL-Password", "Your TelXL Password.")
	api := flag.String("api", "list", "The API command to execute (list - to list them")
	configid := flag.String("configid", "0", "The config ID")
	agentid := flag.String("agentid", "0", "The agent ID")
	release := flag.String("release", "2alpha", "Other values can be 2normal,2alpha,2beta,2rollback")
	title := flag.String("title", "£$%&%^$%$$@@$$$%Testy", "Choose op name or special character")
	table := flag.String("table", "vcc_operators", " Other table can be vcc_operatorgroups")

	flag.Parse()

	config.CONFIGID = *configid
	config.AGENTID = *agentid
	config.RELEASE = *release

	// Make sure we have these on the command line.   Set the API to list, so that it outputs the API useage and Exits
	if config.CONFIGID == "0" {
		fmt.Println("\nYou need to Specfic a ConfigID on the Command Line")
		*api = "list"
	}

	if config.AGENTID == "0" {
		fmt.Println("\nYou need to Specfic a AgentID on the Command Line")
		*api = "list"
	}

	// List and Exit
	if *api == "list" {
		fmt.Println("\n\nUsage:  main -user <username> -pass <password> -api <api command> -configid <number> -agentid <number>")
		fmt.Println("\n\nValid API commands are:")
		fmt.Println("  -api " + utils.Yellow + "list" + utils.Reset + " - Lists all the valid API commands")
		fmt.Println("  -api " + utils.Yellow + "ping" + utils.Reset + " - Sends a PING, waits for a PONG")
		fmt.Println("  -api " + utils.Yellow + "session_test" + utils.Reset + " - Tests the WebSocket Session is active")
		fmt.Println("  -api " + utils.Yellow + "provisioning_request_list" + utils.Reset + " - Get the Current Provisioning List")
		fmt.Println("  -api " + utils.Yellow + "agenttest1" + utils.Reset + " - Run Agent Test 1")
		fmt.Println("  -api " + utils.Yellow + "agentmo" + utils.Reset + " - Run Agent Mo 1 Test Script")
		fmt.Println("  -api " + utils.Yellow + "agentsam" + utils.Reset + " - Run Agent Sam 1 Test Script")
		fmt.Println("")
		os.Exit(0)
	}

	// Load a TelXL Client Struct
	telxlclient := telxlclient.Client{}

	// Run the Methods on the Struct
	telxlclient.Connect(config.URLADDR, config.URLPATH, *user, *pass, *agentid, *configid)
	telxlclient.StartSession(config.RELEASE)

	// At this point we are connected with a valid session, do the API after a 2 second wait for the inital stuff to come in
	time.Sleep(2 * time.Second)

	if *api == "ping" {
		for i := 0; i < 5; i++ {
			telxlclient.SendPing()
			time.Sleep(10 * time.Second)
		}
	}

	if *api == "session_test" {
		telxlclient.SendSessionTest()
	}

	if *api == "provisioning_request_list" {
		telxlclient.SendProvisioningRequestList()
	}

	// if *api == "agenttest1" {
	// 	// TODO: find a better naming convention
	// 	// as there will be a lot of agent scenarios that we will want to test
	// 	telxlclient.Agent1()
	// }

	// if *api == "agentmo" {
	// 	// TODO: find a better naming convention
	// 	// as there will be a lot of agent scenarios that we will want to test
	// 	fmt.Println("API: AgentMo")
	// 	time.Sleep(30 * time.Second)
	// 	telxlclient.AgentMo()

	// 	//  Test ping
	// 	for i := 0; i < 5; i++ {
	// 		telxlclient.SendPing()
	// 	}

	// 	// Test Session
	// 	telxlclient.SendSessionTest()

	// 	// Test Provisioning
	// 	telxlclient.SendProvisioningRequestList()

	// 	time.Sleep(30 * time.Second)

	// 	telxlclient.SendProvisioningGetValues()
	// 	time.Sleep(30 * time.Second)

	// 	//telxlclient.SendProvisioningCreate()
	// 	//time.Sleep(30 * time.Second)

	// 	// End Test - End Session and Disconnect
	// 	telxlclient.EndSession()
	// 	telxlclient.Disconnect()

	// }

	if *api == "agentsam" {

		//test Ping
		telxlclient.SendPing()

		//send provision list
		// telxlclient.SendProvisioningRequestList()

		//Send All provision values
		telxlclient.SendProvisioningValues()

		// Access to set data, "Change the Title of an OP and/or with Special Character".
		telxlclient.SendProvisionTitle(*table, *title)

		// Access to set data, "Create an OP.
		// telxlclient.SendProvisioningCreate()

		// Access to set data: "Send Provisioning Request Telephony Service".
		telxlclient.SendProvisioningRequestTelephonyService()

		// Access to deactivate a GIVEN Op or Group assuming you have created an operator or operatorgroup
		telxlclient.SendProvisionDeactivate()

	}
	time.Sleep(30 * time.Second)
	fmt.Println("All done - Exiting")

	// End the Session and Close
	telxlclient.EndSession()

	telxlclient.Disconnect()

}
