# WebSocket Example Program
## TelXL Limited (Copyright 2020)

This command line utility aims to demo to the WebSocket API connectivity and provides a basic set of tests

# Installation 

```
go get -d
```

# External Libraries used
```
go get "github.com/gorilla/websocket"
go get "github.com/TylerBrock/colorjson"
```

# Basic Usage

When an account is provisioned on the TelXL platform, you will be given: 

username - The Username for the account 
password - The Password for the account

In Addition to the basic credentials, to make any operator based API calls you need a configiddand agent id.

configid - 
agentid - 

```
go run main.go -user <username> -pass <password> -configid <config id> -agentid <agentid > -api <api to call>
```

# To get a list of all the API currently implemented use -api list.

Currently Implemented Tests.  (Selected with the -api command.)

```
session_test                - Logs into the Websocket and Session and tests if it is active. 
provisioning_request_list   - Logs into the WebSocket , creates a session and lists all possible provisioning information
                              This will list all the :
                            
                                operators           - agents on the call center account
                                operator_groups     - groups of agents 
                                numbers             - access numbers
                                users               - users on the account
```

## Yet to be implmented

```
agentsam
agentmo
```




