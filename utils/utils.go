package utils

import (
	"encoding/json"
	"log"

	"github.com/TylerBrock/colorjson"
	"os"
)

const (
	Black   = "\033[1;30m"
	Red     = "\033[1;31m"
	Green   = "\033[1;32m"
	Yellow  = "\033[1;33m"
	Purple  = "\033[1;34m"
	Magenta = "\033[1;35m"
	Teal    = "\033[1;36m"
	White   = "\033[1;37m"
	Reset   = "\033[0m"
)

func Inbound(txt string) {
	log.Output(1, "["+Green+"-->IN"+Reset+"] "+txt)
}

func Outbound(txt string) {
	log.Output(1, "["+Red+"OUT->"+Reset+"] "+txt)
}

func Info(txt string) {
	log.Output(1, "["+Magenta+"INFO "+Reset+"] "+txt)
}

func Handler(txt string) {
	log.Output(1, "["+Teal+"INFO "+Reset+"] "+txt)
}

func Event(txt string) {
	log.Output(1, "["+Yellow+"EVENT"+Reset+"] "+txt)
}

func FormattedJSON(jsonString string) string {

	var obj map[string]interface{}
	json.Unmarshal([]byte(jsonString), &obj)

	// Make a custom formatter with indent set
	f := colorjson.NewFormatter()
	f.Indent = 4

	// Marshall the Colorized JSON
	s, _ := f.Marshal(obj)
	return string(s)
}

// The lazy way to handle errors,  this function checks is there is an error and panics
func Fail(e error, message string) {
	if e != nil {
		log.Println(message, e)
		panic("Aborting..")
	}
}

//exit gracefully - without a stack dump
func Exit(errCode int, message string) {
	log.Println(message)
	os.Exit(errCode)
}
