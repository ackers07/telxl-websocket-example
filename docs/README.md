This folder will hold the details of the TelXL API

Swagger does not support WebSockets so the yaml files are to conform to the AsyncApi format instead
https://www.asyncapi.com/docs/specifications/2.0.0/

A pretty view of the yaml files can be got by pasting the contents into
https://playground.asyncapi.io/

This file needs to be kept up-to-date. So if you do find a command or event that has changed, please update the yaml file accordingly
